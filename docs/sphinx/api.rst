.. _sphinx_api:

#############
|project| API
#############

*********
|project|
*********

.. toctree::
    :maxdepth: 2

.. _micromorphic_element_source:

micromorphic_element.cpp
============

.. doxygenfile:: micromorphic_element.cpp

micromorphic_element.h
==========

.. doxygenfile:: micromorphic_element.h

****************
Abaqus interface
****************

micromorphic_element_umat.cpp
=================

.. doxygenfile:: micromorphic_element_umat.cpp

micromorphic_element_umat.h
===============

.. doxygenfile:: micromorphic_element_umat.h
